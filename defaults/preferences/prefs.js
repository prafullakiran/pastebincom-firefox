pref("extensions.pastebin.username", "");
pref("extensions.pastebin.syntax-highlighting", "text");
pref("extensions.pastebin.post-expiration", "N");
pref("extensions.pastebin.post-exposure", "0");
pref("extensions.pastebin.title", "");
pref("extensions.pastebin.after-paste", "Open");
pref("extensions.pastebin.overwrite-pastes", false);
