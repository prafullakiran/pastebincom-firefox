var pastebindotcom = function() {
  var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
    .getService(Components.interfaces.nsIPrefBranch);
  var passwordManager = Components.classes["@mozilla.org/login-manager;1"]
    .getService(Components.interfaces.nsILoginManager);

  return {

    authStatus: null,
    authKey: null,
    APIKey: 'b7d07cc2908bcd6346361fb48004fcfe',

    initialize: function() {
      pastebindotcom.initEventListeners();
      pastebindotcom.initUI();
    },

    getParamString: function(params) {
      var paramsArray = [];
      for(var key in params) {
        if(params[key]) {
          paramsArray.push(key+"="+encodeURIComponent(params[key]));
        }
      }
      return paramsArray.join("&");
    },

    sendRequest : function(url, params, callback) {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
          callback(xhr.responseText);
        }
      }
      xhr.send(pastebindotcom.getParamString(params));
    },

    contextPopupShowing : function() {
      gContextMenu.showItem("pastebin-context", gContextMenu.isTextSelected);
    },

    getAuthKey : function(username, password) {
      var authMsg = document.getElementById('authMessage');
      var loginButton = document.getElementById('toggleLogin');
      var loginBox = document.getElementById('loginBox');
      var indicator = document.getElementById('indicator');
      var url = 'http://pastebin.com/api/api_login.php';
      var params = {
        'api_dev_key' : pastebindotcom.APIKey,
        'api_user_name' : username,
        'api_user_password' : password
      };
      loginBox.style.display = 'none';
      loginButton.style.display = 'none';
      indicator.style.display = 'block';
      pastebindotcom.sendRequest(url, params, function(response) {
        loginButton.style.display='block';
        indicator.style.display = 'none';
        var pattern = /Bad API Request,/gi;
        if(response.match(pattern)) {
          authMsg.setAttribute('class','authFailure');
          loginButton.value = 'Sign in';
          authMsg.value = 'You are logged in as a guest. Authentication failed';
          pastebindotcom.authStatus = 'Failed';
        } else {
          loginButton.value = 'Sign out';
          authMsg.value = 'You are logged in as ' + username;
          pastebindotcom.authStatus = 'Success';
          pastebindotcom.loginCallback(response);
          pastebindotcom.updateCredentials(username, password);
        }
      });
    },

    getPasswordFor : function(username) {
      var logins = passwordManager.findLogins({}, 'chrome://pastebincom',
                                              null, 'User Authentication');
      var password = null;
      for(var i = 0; i < logins.length; i++) {
        if(logins[i].username == username) {
          password = logins[i].password;
          break;
        }
      }
      return password;
    },

    initEventListeners : function() {
      var menu = document.getElementById("contentAreaContextMenu");
      var authMsg = document.getElementById('authMessage');
      var loginButton = document.getElementById('toggleLogin');
      menu.addEventListener("popupshowing", pastebindotcom.contextPopupShowing, false);

      var username = prefManager.getCharPref("extensions.pastebin.username");
      var password = pastebindotcom.getPasswordFor(username);

      if(username && password) {
        pastebindotcom.getAuthKey(username, password);
      }

      var loginButton = document.getElementById('toggleLogin');
      loginButton.onclick = function() {
        if(loginButton.value == 'Sign in') {
          document.getElementById('loginBox').style.display = 'block';
        } else {
          //signout
          loginButton.value = 'Sign in';
          authMsg.value = 'You are logged in as guest';
          pastebindotcom.logoutCallback();
        }
      };
    },

    initUI : function() {
      //Fill up values from prefs
      var highlight = prefManager.getCharPref("extensions.pastebin.syntax-highlighting");
      var expiration = prefManager.getCharPref("extensions.pastebin.post-expiration");
      var exposure = prefManager.getCharPref("extensions.pastebin.post-exposure");
      var title = prefManager.getCharPref("extensions.pastebin.title");
      document.getElementById('paste-syntax-selector').value = highlight;
      document.getElementById('paste-expiration-selector').value = expiration;
      document.getElementById('paste-privacy-selector').value = exposure;
      document.getElementById('paste-name').value = title;
    },

    updateCredentials: function(username, password) {
      var stored_username = prefManager.getCharPref("extensions.pastebin.username");
      prefManager.setCharPref("extensions.pastebin.username", username);
      var stored_password = pastebindotcom.getPasswordFor(stored_username);
      var nsLoginInfo = new Components.Constructor("@mozilla.org/login-manager/loginInfo;1",
                                                   Components.interfaces.nsILoginInfo,
                                                   "init");
      var loginInfo = new nsLoginInfo('chrome://pastebincom',
                                      null,
                                      'User Authentication',
                                      username, password, '', '');
      if(stored_username == null || stored_password == null) {
        passwordManager.addLogin(loginInfo);
      } else if(stored_username != username || stored_password != password) {
        var oldLoginInfo = new nsLoginInfo('chrome://pastebincom',
                                           null,
                                           'User Authentication',
                                           stored_username, stored_password, '', '');
        passwordManager.modifyLogin(oldLoginInfo, loginInfo);
      }
    },

    loginCallback: function(authKey) {
      pastebindotcom.authKey = authKey;
      //Add option private paste
      var paste_selector = document.getElementById('paste-privacy-selector');
      var private_selector = document.createElement('menuitem');
      private_selector.setAttribute('label', 'Private');
      private_selector.setAttribute('value', '2');
      paste_selector.firstChild.appendChild(private_selector);
    },

    logoutCallback: function() {
      pastebindotcom.authKey = null;
      //Remove option for private paste
      var paste_selector = document.getElementById('paste-privacy-selector');
      var popup_selector = paste_selector.firstChild;
      popup_selector.removeChild(popup_selector.lastChild);
    },

    login: function() {
      document.getElementById('loginBox').style.display = 'none';
      var username = document.getElementById('paste-username').value;
      var password = document.getElementById('paste-password').value;
      pastebindotcom.getAuthKey(username, password);
    },

    cancelLogin: function() {
      document.getElementById('loginBox').style.display = 'none';
    },

    trimStr : function(str) {
      str = str.replace(/^\s+/, "");  // remove leading spaces
      str = str.replace(/\s+$/, "");  // remove trailing spaces
      str = str.replace(/\s+/g, " "); // collapse multiple spaces
      return str;
    },

    getSelectedText : function() {
      var selection = "";
      var focusedElement = document.commandDispatcher.focusedElement;
      if (focusedElement) {
        var selectionStart = focusedElement.selectionStart;
        var selectionEnd   = focusedElement.selectionEnd;
        if (selectionEnd > selectionStart)
          selection = focusedElement.value.substr(selectionStart, selectionEnd);
      } else {
        var focusedWindow = document.commandDispatcher.focusedWindow;
        selection = focusedWindow.getSelection().toString();
      }
      return pastebindotcom.trimStr(selection);
    },

    initPastePanel : function() {
      var keyList = ["loading", "output", "error"];
      for(var i=0; i < keyList.length; i++)
        document.getElementById(keyList[i]+'Box').style.display = 'none';

      var selected_text = pastebindotcom.getSelectedText();
      var overwrite_pastes = prefManager.getBoolPref("extensions.pastebin.overwrite-pastes");
      if(overwrite_pastes) {
        document.getElementById('paste-code').value = selected_text;
      } else {
        var current_paste = document.getElementById('paste-code').value;
        if(current_paste !== "") {
          current_paste += "\n";
        }
        document.getElementById('paste-code').value = current_paste + selected_text;
      }
    },

    copyToClipboard : function() {
      var url = document.getElementById('outputLabel').value;
      try{
        var str = Components.classes["@mozilla.org/supports-string;1"].
          createInstance(Components.interfaces.nsISupportsString);
        if (!str) {
          return false;
        }
        str.data = url;

        var transferable = Components.classes["@mozilla.org/widget/transferable;1"].
          createInstance(Components.interfaces.nsITransferable);
        if (!transferable) {
          return false;
        }

        transferable.addDataFlavor("text/unicode");
        transferable.setTransferData("text/unicode",str,url.length * 2);

        var clipId = Components.interfaces.nsIClipboard;
        var clip = Components.classes["@mozilla.org/widget/clipboard;1"].getService(clipId);
        if (!clip) {
          return false;
        }

        clip.setData(transferable, null, clipId.kGlobalClipboard);
        return true;
      }
      catch(e)
      {
        return false;
      }
    },

    openUrl : function(url) {
      var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
        .getService(Components.interfaces.nsIWindowMediator);
      var mainWindow = wm.getMostRecentWindow("navigator:browser");
      mainWindow.gBrowser.selectedTab = mainWindow.gBrowser.addTab(url);
      //close the popup
      document.getElementById('pastebin-panel').hidePopup();
    },

    updateFooter :function(key, value) {
      var keyList = ["loading", "output", "error"];
      var displayStyle = "none";
      for(var i=0; i < keyList.length; i++) {
        displayStyle = (keyList[i] == key) ? "block" : "none";
        document.getElementById(keyList[i]+'Box').style.display = displayStyle;
      }
      document.getElementById(key+'Label').value = value;
      if(key == 'output') {
        var node = document.getElementById('outputLabel');
        node.onclick = function() {
          pastebindotcom.openUrl(value);
        };
      }
    },

    submitPaste : function() {
      var privacy = 1; //unlisted if we are trying to make a private paste
      var content = document.getElementById('paste-code').value;
      var afterPaste = prefManager.getCharPref("extensions.pastebin.after-paste");
      if(content == '') {
        pastebindotcom.updateFooter('error', 'ERROR: Your paste can not be empty!');
        return;
      }
      var format = document.getElementById('paste-syntax-selector').selectedItem.value;
      var expiration = document.getElementById('paste-expiration-selector').selectedItem.value;

      var privacy_selected_item = document.getElementById('paste-privacy-selector').selectedItem;
      if(privacy_selected_item != null) {
        privacy = privacy_selected_item.value;
      }

      var name = document.getElementById('paste-name').value;

      var url = "http://pastebin.com/api/api_post.php";
      var params = {
        'api_dev_key' : pastebindotcom.APIKey,
        'api_option' : 'paste',
        'api_paste_code' : content,
        //optional params now
        'api_user_key' : pastebindotcom.authKey,
        'api_paste_name' : name,
        'api_paste_format' : format,
        'api_paste_private' : privacy,
        'api_paste_expire_date' : expiration,
      }
      pastebindotcom.sendRequest(url, params, function(response) {
        document.getElementById('paste-code').value = "";
        if(afterPaste == 'Open')
          pastebindotcom.openUrl(response);
        else //afterPaste == 'Show'
          pastebindotcom.updateFooter("output", response);
      });
      pastebindotcom.updateFooter("loading", "Loading...");
    }
  };
}();
window.addEventListener("load", pastebindotcom.initialize, false);
